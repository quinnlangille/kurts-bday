	//This function checks to see if today is Kurt's Birthday. 

	window.onload = function bday() {

		var today = new Date(); // This gets today's date
		var day = today.getDate();
		var month = today.getMonth(); // Note: January = 0

		// This uses the above variables to check if today is March 16th, and if it is changes the text in #bday-status
		if (month == 2 && day == 16) {
			document.getElementById("bday-status").innerHTML = "Truck yeah!";
		} else {
			document.getElementById("bday-status").innerHTML = "Nah :(";
		}
	};
